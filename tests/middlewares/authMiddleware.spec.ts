import authMiddleware from "../../src/middlewares/authMiddleware";
import jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";

describe("authMiddleware", () => {
  let mockReq: Partial<Request> & { userId?: string };
  let mockRes: Partial<Response>;
  let mockNext: NextFunction;

  beforeEach(() => {
    mockReq = {};
    mockRes = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    mockNext = jest.fn();
  });

  test("should return 401 if no token", () => {
    authMiddleware(mockReq as Request, mockRes as Response, mockNext);

    expect(mockRes.status).toHaveBeenCalledWith(401);
    expect(mockRes.json).toHaveBeenCalledWith({ error: "Unauthorized" });
  });

  test("should return 401 if invalid token", () => {
    mockReq.header = jest.fn().mockReturnValue("Bearer invalidtoken");

    authMiddleware(mockReq as Request, mockRes as Response, mockNext);

    expect(mockRes.status).toHaveBeenCalledWith(401);
    expect(mockRes.json).toHaveBeenCalledWith({ error: "Unauthorized" });
  });

  test("should call next if valid token", () => {
    const userId = "user123";
    const token = jwt.sign({ userId }, "secret");
    process.env.JWT_SECRET = "secret";

    mockReq.header = jest.fn().mockReturnValue(`Bearer ${token}`);

    authMiddleware(mockReq as Request, mockRes as Response, mockNext);

    expect(mockReq.userId).toBe(userId);
    expect(mockNext).toHaveBeenCalled();
  });
});
