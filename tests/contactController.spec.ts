import { Response } from "express";
import * as contactController from "../src/controllers/contactController";
import Contact from "../src/models/Contact";

jest.mock("../src/models/Contact");

describe("contactController", () => {
  describe("addContact", () => {
    test("should create a new contact", async () => {
      const req = {
        body: {
          firstName: "John",
          lastName: "Doe",
          phoneNumber: "123-456-7890",
          email: "john@test.com",
          birthdate: "1990-01-01",
        },
        userId: "user123",
      };

      const res = {
        json: jest.fn(),
      } as unknown as Response;

      const mockContact = {
        _id: "contact123",
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        phoneNumber: req.body.phoneNumber,
        email: req.body.email,
        birthdate: req.body.birthdate,
        user: req.userId,
      };

      Contact.create = jest.fn().mockResolvedValue(mockContact);

      await contactController.addContact(req, res);

      expect(Contact.create).toHaveBeenCalledWith({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        phoneNumber: req.body.phoneNumber,
        email: req.body.email,
        birthdate: req.body.birthdate,
        user: req.userId,
      });

      expect(res.json).toHaveBeenCalledWith({
        contact: mockContact,
      });
    });
  });

  describe("listContacts", () => {
    test("should return paginated contacts", async () => {
      const req = {
        query: {
          page: 2,
          limit: 10,
        },
        userId: "user123",
      };

      const res = {
        json: jest.fn(),
        status: jest.fn(),
      } as unknown as Response;

      const mockContacts = [
        { _id: "1", name: "John" },
        { _id: "2", name: "Jane" },
      ];

      // mock the pagination of Contacts
      Contact.find = jest.fn().mockReturnValue({
        skip: jest.fn().mockReturnValue({
          limit: jest.fn().mockResolvedValue(mockContacts),
        }),
      });

      Contact.countDocuments = jest.fn().mockResolvedValue(50);

      await contactController.listContacts(req, res);

      expect(Contact.find).toHaveBeenCalledWith({
        user: req.userId,
      });

      expect(Contact.countDocuments).toHaveBeenCalledWith({
        user: req.userId,
      });
    });
  });

  describe("getContactById", () => {
    test("should return 404 if contact not found", async () => {
      const req = {
        params: {
          id: "123",
        },
        userId: "user123",
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      } as unknown as Response;

      Contact.findOne = jest.fn().mockResolvedValue(null);

      await contactController.getContactById(req, res);

      expect(res.status).toHaveBeenCalledWith(404);
      expect(res.json).toHaveBeenCalledWith({ error: "Contact not found" });
    });

    test("should return contact if found", async () => {
      const req = {
        params: {
          id: "123",
        },
        userId: "user123",
      };

      const res = {
        json: jest.fn(),
      } as unknown as Response;

      const mockContact = {
        _id: "123",
        name: "John",
      };

      Contact.findOne = jest.fn().mockResolvedValue(mockContact);

      await contactController.getContactById(req, res);

      expect(res.json).toHaveBeenCalledWith({
        contact: mockContact,
      });
    });
  });

  describe("deleteContact", () => {
    test("should return 404 if contact not found", async () => {
      const req = {
        params: { id: "123" },
        userId: "user123",
      };

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      } as unknown as Response;

      Contact.findOneAndDelete = jest.fn().mockResolvedValue(null);

      await contactController.deleteContact(req, res);

      expect(res.status).toHaveBeenCalledWith(404);
      expect(res.json).toHaveBeenCalledWith({ error: "Contact not found" });
    });

    test("should delete contact if found", async () => {
      const req = {
        params: { id: "123" },
        userId: "user123",
      };

      const res = {
        json: jest.fn(),
      } as unknown as Response;

      const deletedContact = {
        _id: "123",
        name: "John Doe",
      };

      Contact.findOneAndDelete = jest.fn().mockResolvedValue(deletedContact);

      await contactController.deleteContact(req, res);

      expect(res.json).toHaveBeenCalledWith({
        message: "Contact deleted successfully",
      });
    });
  });
});
