import { Request, Response } from "express";
import * as authController from "../src/controllers/authController";
import User from "../src/models/User";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

jest.mock("../src/models/User");
jest.mock("bcrypt");
jest.mock("jsonwebtoken");

describe("authController", () => {
  describe("createAccount", () => {
    test("should create a new user and return a token", async () => {
      const mockUser = {
        _id: "123",
        fullName: "John Doe",
        phoneNumber: "555-1234",
        email: "john@email.com",
        password: "password123",
      };

      bcrypt.hash = jest.fn().mockResolvedValue("hashedPassword");

      User.create = jest.fn().mockResolvedValue(mockUser);

      jwt.sign = jest.fn().mockReturnValue("token123");

      const req = {
        body: {
          fullName: mockUser.fullName,
          phoneNumber: mockUser.phoneNumber,
          email: mockUser.email,
          password: mockUser.password,
        },
      } as Request;

      const res = {
        json: jest.fn(),
      } as unknown as Response;

      await authController.createAccount(req, res);

      expect(bcrypt.hash).toHaveBeenCalledWith(mockUser.password, 10);
      expect(User.create).toHaveBeenCalledWith({
        fullName: mockUser.fullName,
        phoneNumber: mockUser.phoneNumber,
        email: mockUser.email,
        password: "hashedPassword",
      });
      expect(jwt.sign).toHaveBeenCalledWith(
        { userId: mockUser._id },
        process.env.JWT_SECRET,
        { expiresIn: "4h" }
      );
      expect(res.json).toHaveBeenCalledWith({ token: "token123" });
    });
  });

  describe("signIn", () => {
    test("should return 401 if user not found", async () => {
      const req = {
        body: {
          email: "john@email.com",
          password: "password123",
        },
      } as Request;

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      } as unknown as Response;

      User.findOne = jest.fn().mockResolvedValue(null);

      await authController.signIn(req, res);

      expect(res.status).toHaveBeenCalledWith(401);
      expect(res.json).toHaveBeenCalledWith({ error: "Invalid credentials" });
    });

    test("should return 401 if invalid password", async () => {
      const req = {
        body: {
          email: "john@email.com",
          password: "password123",
        },
      } as Request;

      const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      } as unknown as Response;

      const mockUser = {
        password: "hashedPassword",
      };

      User.findOne = jest.fn().mockResolvedValue(mockUser);

      bcrypt.compare = jest.fn().mockResolvedValue(false);

      await authController.signIn(req, res);

      expect(res.status).toHaveBeenCalledWith(401);
      expect(res.json).toHaveBeenCalledWith({ error: "Invalid credentials" });
    });

    test("should return token if valid credentials", async () => {
      const req = {
        body: {
          email: "john@email.com",
          password: "password123",
        },
      } as Request;

      const res = {
        json: jest.fn(),
      } as unknown as Response;

      const mockUser = {
        _id: "123",
        password: "hashedPassword",
      };

      User.findOne = jest.fn().mockResolvedValue(mockUser);

      bcrypt.compare = jest.fn().mockResolvedValue(true);

      jwt.sign = jest.fn().mockReturnValue("token123");

      await authController.signIn(req, res);

      expect(jwt.sign).toHaveBeenCalledWith(
        { userId: mockUser._id },
        process.env.JWT_SECRET,
        { expiresIn: "4h" }
      );
      expect(res.json).toHaveBeenCalledWith({ token: "token123" });
    });
  });
});
