import { validateRequest } from "../../src/utils/validateRequest";
import { Request, Response, NextFunction } from "express";
import * as classValidator from "class-validator";
jest.mock("class-validator", () => ({
  validate: jest.fn(),
}));
describe("validateRequest", () => {
  let mockReq: Partial<Request>;
  let mockRes: Partial<Response>;
  let mockNext: NextFunction;

  beforeEach(() => {
    mockReq = {};
    mockRes = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    mockNext = jest.fn();
  });

  test("should return 400 if validation fails", async () => {
    const mockDto = {} as any;
    const mockErrors = [
      { constraints: { isRequired: "Field is required" } },
    ] as unknown as classValidator.ValidationError[];
    jest.spyOn(classValidator, "validate").mockResolvedValueOnce(mockErrors);

    await validateRequest(
      mockReq as Request,
      mockRes as Response,
      mockNext,
      mockDto
    );

    expect(mockRes.status).toHaveBeenCalledWith(400);
    expect(mockRes.json).toHaveBeenCalledWith({
      errors: ["Field is required"],
    });
  });

  test("should call next if validation passes", async () => {
    const mockDto = {} as any;

    jest.spyOn(classValidator, "validate").mockResolvedValueOnce([]);

    await validateRequest(
      mockReq as Request,
      mockRes as Response,
      mockNext,
      mockDto
    );

    expect(mockNext).toHaveBeenCalled();
  });
});
