# Contacts-Manager [Bringy Task]

This repository contains a Node.js/Express.js back-end application that utilizes a NoSQL database. The application allows users to create an account, sign in, manage their address book contacts, and perform various operations on them.

## Table of Contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
- [API Endpoints](#api-endpoints)
- [Testing](#testing)
- [Deployment with Docker Compose](#deployment-with-docker-compose)
- [Additional Features](#additional-features)


## Requirements

To fulfill the given task, the application adheres to the following requirements:

- Developed using Node.js and Express.js.
- Written in TypeScript.
- Utilizes a NoSQL database.
- Provides secure user authentication and data protection.
- Well-documented code.
- No front-end application is required, only the back-end.
 
## Installation

To install and set up the application locally, please follow these steps:

1- Clone the repository to your local machine:

```bash
    git clone https://github.com/your-username/node-express-nosql-app.git
```

2- Navigate to the project directory:

```bash
    cd node-express-nosql-app
```

3- Install the required dependencies using npm (Node Package Manager):

```bash
    npm install
```

4- Configure the environment variables. Create a ```.env``` file in the root directory of the project and provide the necessary configuration variables. You can use the provided ```.env.example``` file as a template.

5- Set up your NoSQL database. Follow the documentation for your chosen database and configure the connection details in the ```.env``` file. or you can use docker to run the database container

```bash
    docker run -d -p 27017:27017 -v ~/data:/data/db mongo
```

## Build the application:

```bash
    npm run build
```

## Usage

Before running the application, make sure you have completed the installation steps.

To start the application, run the following command:

```bash
    npm start
```

By default, the application will be accessible at http://localhost:3000.

## API Endpoints

The following API endpoints are available:

1- ```POST``` **/api/auth/create-account**: Create a new user account.

- Body parameters: all fields are required

```json
    {
        "fullName": "Amr Monier",
        "phoneNumber": "+20101000000",
        "email": "a.example@gmail.com",
        "password": "1234Aa1234"
    }
```

- Response Example

```json
    {
        "token": "jwt token"
    }
```

2- ```POST``` **/api/auth/sign-in**: Sign in with an existing user account and receive a JWT token.

- Body parameters: all fields are required

```json
    {
        "email": "a.example@gmail.com",
        "password": "Test1234"
    }
```

- Response Example

```json
    {
        "token": "jwt token"
    }
```

3- ```POST``` **/api/contacts**: Add a contact to the user's address book.

- Body parameters: all fields are required

```json
    {
        "firstName": "Amr",
        "lastName": "Monier",
        "email": "a.example@gmail.com",
        "birthdate": "1998-06-09",
        "phoneNumber": "+20101000000"
    }
```

- Authorization: Bearer Token

- Response Example

```json
   {
        "contact": {
            "firstName": "Amr",
            "lastName": "Monier",
            "phoneNumber": "+20101000000",
            "email": "a.example@gmail.com",
            "birthdate": "1998-06-09T00:00:00.000Z",
            "user": "64b5bf825b6df8412c430e39",
            "_id": "64b5bfe95b6df8412c430e3b",
            "createdAt": "2023-07-17T22:25:45.630Z",
            "updatedAt": "2023-07-17T22:25:45.630Z",
            "__v": 0
        }
    }
```

4- ```GET``` **/api/contacts**: Get a list of all contacts in the user's address book paginated.

- Query parameters: page and limit

```json
    {
        "page": 1, // default 1
        "limit": 5 // default 10
    }
```

- Authorization: Bearer Token

- Response Example

```json
    {
        "data": [
            {
                "_id": "64b5bfe95b6df8412c430e3b",
                "firstName": "Amr",
                "lastName": "Monier",
                "phoneNumber": "+201000000000",
                "email": "example@gmail.com",
                "birthdate": "1990-05-19T00:00:00.000Z",
                "user": "64b5bf825b6df8412c430e39",
                "createdAt": "2023-07-17T22:25:45.630Z",
                "updatedAt": "2023-07-17T22:25:45.630Z",
                "__v": 0
            }
        ],
        "meta": {
            "currentPage": 1,
            "limit": 5,
            "totalPages": 1,
            "total": 1
        }
}
```

5- ```GET``` **/api/contacts/:id**: Get a specific contact by ID.

- Authorization: Bearer Token

- Response Example

```json
    {
        "contact": {
            "_id": "64b5bfe95b6df8412c430e3b",
            "firstName": "Amr",
            "lastName": "Monier",
            "phoneNumber": "+20101000000",
            "email": "example@gmail.com",
            "birthdate": "1998-06-09T00:00:00.000Z",
            "user": "64b5bf825b6df8412c430e39",
            "createdAt": "2023-07-17T22:25:45.630Z",
            "updatedAt": "2023-07-17T22:25:45.630Z",
            "__v": 0
        }
    }
```

- Throws an error  (404) if the contact does not exist. 

```json
    {
    "error": "Contact not found"
    }
```

6- ```DELETE``` **/api/contacts/:id**: Delete a contact from the user's address book.

- Authorization: Bearer Token

- Response Example

```json
    {
        "message": "Contact deleted successfully"
    }
```

- Throws an error  (404) if the contact does not exist.

```json
    {
        "error": "Contact not found"
    }
```

## Testing

The application includes a test suite to validate its functionality. To run the tests, use the following command:

```bash
    npm test
```

The test suite performs logic tests for the application's endpoints.

## Deployment with Docker Compose

To deploy the application using Docker Compose, follow these steps:

1- Install Docker and Docker Compose on your machine.

2- Build and run the Docker containers:

```bash
    docker-compose up --build
    # or for some other versions of docker
    docker compose up --build 
```

This will start the application and the database containers.

3- Access the application at the specified port (e.g., http://localhost:3000).

## Additional Features

The application includes the following optional additional feature:

- Paging ability for retrieving all user contacts in chunks.
