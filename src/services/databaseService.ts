import mongoose from "mongoose";

const connectDatabase = async () => {
  try {
    const dbUri = process.env.DB_URI;

    if (!dbUri) {
      throw new Error("Database URI not provided");
    }

    await mongoose.connect(dbUri, {});

    console.log("Connected to the database");
  } catch (error) {
    console.error("Failed to connect to the database:", error);
    process.exit(1);
  }
};

export { connectDatabase };
