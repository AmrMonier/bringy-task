import mongoose, { Document, Schema } from "mongoose";

export interface UserDocument extends Document {
  fullName: string;
  phoneNumber: string;
  email: string;
  password: string;
}

const userSchema = new Schema<UserDocument>(
  {
    fullName: { type: String, required: true },
    phoneNumber: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
  },
  { timestamps: true }
);

const User = mongoose.model<UserDocument>("User", userSchema);

export default User;
