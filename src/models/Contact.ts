import mongoose, { Document, Schema } from "mongoose";
import { UserDocument } from "./User";

export interface ContactDocument extends Document {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  birthdate: Date;
  user: UserDocument["_id"];
}

const contactSchema = new Schema<ContactDocument>(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    phoneNumber: { type: String, required: true },
    email: { type: String, required: true },
    birthdate: { type: Date, required: true },
    user: { type: Schema.Types.ObjectId, ref: "User", required: true },
  },
  { timestamps: true }
);

const Contact = mongoose.model<ContactDocument>("Contact", contactSchema);

export default Contact;
