import { Request, Response, NextFunction } from "express";
import { validate, ValidationError } from "class-validator";
import { plainToClass } from "class-transformer";

export const validateRequest = async (
  req: Request,
  res: Response,
  next: NextFunction,
  dtoClass: any
) => {
  try {
    const dto = plainToClass(dtoClass, req.body);

    // Validate the DTO using class-validator
    const errors: ValidationError[] = await validate(dto);
    
    if (errors.length > 0) {
      const errorMessages = errors.map((error) =>
        error.constraints ? Object.values(error.constraints) : null
      );
      return res.status(400).json({ errors: errorMessages });
    }

    // Attach the validated DTO to the request object

    next();
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};
