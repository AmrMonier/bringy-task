import express, { NextFunction, Request, Response } from "express";
import { createAccount, signIn } from "../controllers/authController";
import { validateRequest } from "../utils/validateRequest";
import { CreateAccountDto } from "../dtos/auth/createAccountDto";
import { SignInDto } from "../dtos/auth/signInDto";

const router = express.Router();
// add prefix /auth to all routes
router.get("/test", (req, res) => {
  res.send("Hello World");
});
router.post(
  "/create-account",
  (req: Request, res: Response, next: NextFunction) =>
    validateRequest(req, res, next, CreateAccountDto),
  createAccount
);
router.post(
  "/sign-in",
  (req: Request, res: Response, next: NextFunction) =>
    validateRequest(req, res, next, SignInDto),

  signIn
);
export default router;
