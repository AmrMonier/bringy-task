import express, { NextFunction, Request, Response } from "express";
import {
  addContact,
  listContacts,
  getContactById,
  deleteContact,
} from "../controllers/contactController";
import authMiddleware from "../middlewares/authMiddleware";
import { validateRequest } from "../utils/validateRequest";
import { CreateContactDto } from "../dtos/contact/CreateContactDto";

const router = express.Router();

router.post(
  "/",
  authMiddleware,
  (req: Request, res: Response, next: NextFunction) =>
    validateRequest(req, res, next, CreateContactDto),
  addContact
);
router.get("/", authMiddleware, listContacts);
router.get("/:id", authMiddleware, getContactById);
router.delete("/:id", authMiddleware, deleteContact);

export default router;
