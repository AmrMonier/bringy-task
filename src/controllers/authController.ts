import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import User from "../models/User";

const createAccount = async (req: Request, res: Response) => {
  const { fullName, phoneNumber, email, password } = req.body;

  // Hash the password
  const hashedPassword = await bcrypt.hash(password, 10);

  // Create the user account
  const user = await User.create({
    fullName,
    phoneNumber,
    email,
    password: hashedPassword,
  });

  // Generate JWT token
  const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET!, {
    expiresIn: "4h",
  });

  res.json({ token });
};

const signIn = async (req: Request, res: Response) => {
  const { email, password } = req.body;

  // Find the user by email
  const user = await User.findOne({ email });

  // check if the user exist if not throw a 401
  if (!user) {
    return res.status(401).json({ error: "Invalid credentials" });
  }

  // Verify the password
  const passwordMatch = await bcrypt.compare(password, user.password);

  if (!passwordMatch) {
    return res.status(401).json({ error: "Invalid credentials" });
  }

  // Generate JWT token
  const token = jwt.sign({ userId: user?._id }, process.env.JWT_SECRET!, {
    expiresIn: "4h",
  });

  res.json({ token });
};

export { createAccount, signIn };
