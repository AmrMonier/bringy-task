import { Response } from "express";
import Contact from "../models/Contact";
import { Types } from "mongoose";
const addContact = async (req: any, res: Response) => {
  const { firstName, lastName, phoneNumber, email, birthdate } = req.body;

  // Validate input
  // ...
  console.log(req.userId);

  // Create the contact
  const contact = await Contact.create({
    firstName,
    lastName,
    phoneNumber,
    email,
    birthdate,
    user: req.userId,
  });

  res.json({ contact });
};

const listContacts = async (req: any, res: Response) => {
  const page = parseInt(req.query.page) || 1;
  const limit = parseInt(req.query.limit) || 10;
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const contacts = await Contact.find({ user: req.userId })
    .skip(startIndex)
    .limit(limit);
  // ToDo:: add sorting
  // sorting can be added by converting the request to a post request and adding a sort object to the body
  // sort object would be something like { firstName: 1 } or { firstName: -1 }
  // this way we can enable sorting by only fields allowed by the validator to avoid the user entering wrong data

  const total = await Contact.countDocuments({ user: req.userId });

  const pagination = {
    currentPage: page,
    limit,
    totalPages: Math.ceil(total / limit),
    total,
  };

  res.json({
    data: contacts,
    meta: pagination,
  });
};

const getContactById = async (req: any, res: Response) => {
  const { id } = req.params;
  // validate id to be a mongo id
  if (!Types.ObjectId.isValid(id)) {
    return res.status(404).json({ error: "Contact not found" });
  }

  const contact = await Contact.findOne({ _id: id, user: req.userId });

  if (!contact) {
    return res.status(404).json({ error: "Contact not found" });
  }

  res.json({ contact });
};

const deleteContact = async (req: any, res: Response) => {
  const { id } = req.params;
  if (!Types.ObjectId.isValid(id)) {
    return res.status(404).json({ error: "Contact not found" });
  }
  const contact = await Contact.findOneAndDelete({
    _id: id,
    user: req.userId,
  });

  if (!contact) {
    return res.status(404).json({ error: "Contact not found" });
  }

  res.json({ message: "Contact deleted successfully" });
};

export { addContact, listContacts, getContactById, deleteContact };
