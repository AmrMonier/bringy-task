import {
  IsEmail,
  IsNotEmpty,
  IsPhoneNumber,
  IsString,
  IsStrongPassword,
  Length,
} from "class-validator";

export class CreateAccountDto {
  @IsNotEmpty()
  @IsString()
  @Length(3, 255)
  fullName!: string;

  @IsNotEmpty()
  @IsString()
  @IsPhoneNumber()
  phoneNumber!: string;

  @IsNotEmpty()
  @IsEmail()
  email!: string;

  @IsNotEmpty()
  @IsStrongPassword(
    {
      minSymbols: 0,
      minLength: 8,
      minLowercase: 1,
      minUppercase: 1,
    },
    {
      message:
        "password must contain at least 1 uppercase letter and 1 lowercase letter and one number",
    }
  )
  @Length(8, 12)
  password!: string;
}
