import {
  IsDate,
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsPhoneNumber,
  IsString,
  Length,
} from "class-validator";

export class CreateContactDto {
  @IsNotEmpty()
  @IsString()
  @Length(3, 255)
  firstName!: string;
  @IsNotEmpty()
  @IsString()
  @Length(3, 255)
  lastName!: string;
  @IsNotEmpty()
  @IsString()
  @IsPhoneNumber()
  phoneNumber!: string;
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email!: string;
  @IsNotEmpty()
  @IsDateString()
  birthdate!: Date;
}
