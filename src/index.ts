import express, { NextFunction, Request, Response } from "express";
import dotenv from "dotenv";
import app from "./app";
import { connectDatabase } from "./services/databaseService";

dotenv.config();

const port = process.env.PORT || 3000;

// Connect to the database
connectDatabase();



// Error handling middleware

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
