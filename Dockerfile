FROM node:18

WORKDIR /app

COPY package*.json ./

RUN npm install 

COPY . .

# CMD ["npm", "run", "build"]

EXPOSE 3000

RUN npm run build

CMD [ "npm", "run", "start" ]
